import {v4 as uuidv4} from "uuid";
import executeQuery from "./db.js";
import queries from "./queries.js";

export default {
    insertProduct: async (name, price) => {
        const id = uuidv4();
        const params = [id, name, price];
        console.log("Inserting a new product");
        const result = await executeQuery(queries.insertProduct, params);
        if(result.rowCount !== 1){
            throw "No rows affected";
        }
        console.log(`New product ${id} inserted succesufully.`);
        return {id, name, price};
    },
    
    findAll: async () => {
        console.log("Finding all products");
        const result = await executeQuery(queries.findAllProducts);
        return result;
    },
    
    findOne: async (id) => {
        console.log(`Finding product with id:${id}`);
        const result = await executeQuery(queries.findProduct, [id]);
        return result;
    },
    
    updateProduct: async (id, name, price) => {
        console.log(`Updating product with id:${id}`);
        const params = [name, price, id];
        const result = await executeQuery(queries.updateProduct, params);
        console.log(`Product ${id} updated succesufully.`);
        return result;
    },

    removeProduct: async (id) => {
        console.log(`Removing product with id:${id}`);
        const result = await executeQuery(queries.deleteProduct, [id]);
        console.log(`Product ${id} removed succesufully.`);
        return result;
    }
};