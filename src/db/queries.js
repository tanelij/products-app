const createProductTable = `
    CREATE TABLE IF NOT EXISTS "products" (
        "id" VARCHAR(100) NOT NULL,
        "name" VARCHAR(100) NOT NULL,
        "price" INTEGER NOT NULL,
        PRIMARY KEY ("id")
    )`;

const insertProduct = `
    INSERT INTO products (id, name, price)
    VALUES ($1, $2, $3::integer)
`;

const findAllProducts = `
    SELECT * FROM products
`;

const findProduct = `
    SELECT * FROM products
    WHERE id=$1
`;

const updateProduct = `
    UPDATE products
    SET name=$1, price=$2
    WHERE id=$3
`;

const deleteProduct = `
    DELETE FROM products
    WHERE id=$1
`;

export default {createProductTable, insertProduct, findAllProducts, findProduct, updateProduct, deleteProduct};