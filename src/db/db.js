import pg from "pg";
import queries from "./queries.js";

const isProd = process.env.NODE_ENV === "production";

const {APP_PG_HOST, APP_PG_PORT, APP_PG_USER, APP_PG_PASSWORD, APP_PG_DB} = process.env;

export const pool = new pg.Pool({
    host: APP_PG_HOST,
    port: APP_PG_PORT,
    user: APP_PG_USER,
    password: APP_PG_PASSWORD,
    database: APP_PG_DB,
    ssl: isProd
});

export const executeQuery = async (query, parameters) => {
    const client = await pool.connect();
    console.log(client);
    try{
        const result = await client.query(query, parameters);
        //const result = await client.query(query);

        return result;
    }catch(error){
        console.error(error.stack);
        error.name = "dbError";
        throw error;
    }finally{
        client.release();
    }
};

export const createProductTable = async () => {
    if(process.env.NODE_ENV !== "test"){
        await executeQuery(queries.createProductTable);
        console.log("Product table initialized successfully.");
    }
};

export default executeQuery;