import server from "./api/server.js";
const PORT = process.env.APP_PORT || 3000;


server.listen(PORT, () => {
    console.log(`Server listening on port: ${PORT}`);
    console.log(`Environment variables:`, Object.entries(process.env).filter(([key, value]) => key.includes("APP_")));
});
