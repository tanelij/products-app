import express from "express";
import {createProductTable} from "../db/db.js";
import productsRouter from "./products.js";

createProductTable();
const server = express();
server.use("/products", productsRouter);

export default server;