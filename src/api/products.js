import express from "express";
import dao from "../db/dao.js";

const router = express.Router();
router.use(express.json());
router.use(errorHandler);
function errorHandler (err, req, res, next) {
    if (res.headersSent) {
        return next(err);
    }
    res.status(500);
}

router.post("/", async (req, res) => {
    const {name, price} = req.body;
    const result = await dao.insertProduct(name, price);
    res.json(result);
});

router.get("/", async (req, res) => {
    const result = await dao.findAll();
    res.json(result.rows);
});

router.get("/:id", async (req, res) => {
    const result = await dao.findOne(req.params.id);
    const product = result.rows[0];
    res.json(product);
});

router.put("/:id", async (req, res) => {
    const {name, price} = req.body;
    const result = await dao.updateProduct(req.params.id, name, price);
    res.json(result.rows);
});

router.delete("/:id", async (req, res) => {
    const result = await dao.removeProduct(req.params.id);
    res.json(result.rows);
});

export default router;