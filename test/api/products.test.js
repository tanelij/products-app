import {jest} from "@jest/globals";
import request from "supertest";
import {pool} from "../../src/db/db.js";
import dao from "../../src/db/dao.js";
import server from "../../src/api/server.js";

const mockResponse = {
    rows: [
        {
            id: "12345", name: "tv", price:"3000",
        },
        {
            id: "23456", name: "fridge", price: "600"
        }
    ]
};

const initMock = (expectedResponse) => {
    pool.connect = jest.fn(() => {
        return {
            query: () => {
                return expectedResponse;
            },
            release: () => null
        };
    });

    dao.insertProduct = jest.fn(() => {
        return expectedResponse;
    });
};

describe("Testing get /products", () => {
    beforeAll(() => {
        initMock(mockResponse);
    });
    afterEach(() => {
        jest.clearAllMocks();
    });
    it("Should return 200 with all products", async () => {
        const response = await request(server)
            .get("/products")
            .set("Content-type", "application/json");
        expect(response.status).toBe(200);
        expect(response.body).toStrictEqual(mockResponse.rows);
    });
});

describe("Testing post /products", () => {
    afterEach(() => {
        jest.clearAllMocks();
    });
    it("Should return 200 when posting product", async () => {
        const product = {
            "name": "toothbrush",
            "price": "3"
        };
        const expectedResponse = {
            id: "12345",
            ...product
        };
        initMock(expectedResponse);
        const response = await request(server)
            .post("/products")
            .send(product)
            .set("Content-type", "application/json");
        expect(response.status).toBe(200);
        expect(response.body).toStrictEqual(expectedResponse);
    }, 900000);
});